package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int amountInFridge;
	int fridgeSize;
	List<FridgeItem> itemsInFridge = new ArrayList<FridgeItem>();


	public Fridge() {
		this.amountInFridge = 0;
		this.fridgeSize = 20;
	}
	public int nItemsInFridge(){
    	this.amountInFridge = itemsInFridge.size();
		return this.amountInFridge;
    }
	public int totalSize(){
		return fridgeSize;
	}
	public boolean placeIn(FridgeItem item){
		if (itemsInFridge.size()>=fridgeSize){
			return false;
		}else{
			itemsInFridge.add(item);
			//System.out.println(itemsInFridge);
			return true;
		}
	}
	public void takeOut(FridgeItem item){
		if (itemsInFridge.contains(item)){
			itemsInFridge.remove(item);
		}else{
			throw new NoSuchElementException();
		}
	}
	public void emptyFridge(){
		itemsInFridge.clear();
	}
	public List<FridgeItem> removeExpiredFood(){
		List<FridgeItem> expiredFridgeItems = new ArrayList<FridgeItem>();
		for (FridgeItem i : itemsInFridge){
			if (i.hasExpired()){
				expiredFridgeItems.add(i);
			}
		}
		itemsInFridge.removeAll(expiredFridgeItems);
		amountInFridge = itemsInFridge.size();
		return expiredFridgeItems;
		}
	}



